# Guide till att översätta debconf po

# Välj ett paket
https://www.debian.org/international/l10n/po-debconf/sv

# Kontrollera att ingen redan har skickat en felrapport
https://bugs.debian.org/<paketnamn>

# Kontrollera att ingen annan påbörjat översättning (ITT)
https://l10n.debian.org/coordination/swedish/sv.by_status.html

# Skicka avsikt att översätta
E-post till: debian-l10n-swedish@lists.debian.org
Ärenderad: [ITT] po-debconf://<paketnamn>/sv.po

# Ladda ner pot-fil
https://www.debian.org/international/l10n/po-debconf/pot

# Redigera och spara som sv.po

# Kontrollera formatfel
msgfmt -c -v -o /dev/null sv.po

# Skicka begäran om korrekturläsning
E-post till: debian-l10n-swedish@lists.debian.org
Ärenderad: [RFR] po-debconf://<paketnamn>/sv.po

# Omarbeta översättningen och upprepa föregående steg vid behov

# Skicka in felrapport
reportbug
Ärenderad: [INTL:sv] Swedish translation of debconf messages
Severity: wishlist
Message:
"
Dear Maintainer,
Please copy the attachment into debian/po/sv.po
It has been reviewed by the Swedish language team,
tested with msgfmt -c -v -o /dev/null sv.po, and is in UTF-8.

Regards,
<namn>
"
t=tags
5
7
a=attach file
<file>
Y=skicka

# Vänta på konfirmation och fel-ID per e-post från BTS

# Skicka fel-ID till e-postlistan
Mail till debian-l10n-swedish@lists.debian.org
Ärenderad: [BTS#<fel-ID>] po-debconf://<paketnamn>/sv.po

